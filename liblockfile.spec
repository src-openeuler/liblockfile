Name:           liblockfile
Version:        1.17
Release:        3
Summary:        Library providing functions to lock standard mailboxes
License:        GPL-2.0-or-later AND LGPL-2.0-or-later
URL:            https://github.com/miquels/liblockfile
Source0:        https://github.com/miquels/liblockfile/archive/v%{version}.tar.gz

BuildRequires:  gcc make
BuildRequires:  ldconfig

%description
This library implements a number of functions found in -lmail on SysV
systems.

%package       devel
Summary:       Liblockfile development files
Requires:      liblockfile = %{version}-%{release}

%description   devel
The liblockfile-devel package contains libraries and header files for
liblockfile development.

%package_help

%prep
%autosetup -n liblockfile-%{version} -p1

# There are occurrences of "install -g GROUP ...".
#
# Changing the group requires permissions that are normally not
# available while packaging.
#
# Let's remove "-g GROUP".
sed -Ei "/install/ s/-g [^ ]+//" Makefile.in

# Makefile.in mixes and messes with DESTDIR and prefix.
# See the following pull requests submitted upstream:
# https://github.com/miquels/liblockfile/pull/11
# https://github.com/miquels/liblockfile/pull/15
sed -i \
    -e '/^prefix/s,\$(DESTDIR),,' \
    -e 's,\(\$(\(lib\|include\|man\|nfslock\|bin\)dir)\),$(DESTDIR)\1,' \
    -e '/-DLOCKPROG/s,\$(DESTDIR),,' Makefile.in

%build
%configure --enable-shared
%make_build

%install
%make_install

/sbin/ldconfig -N -n %{buildroot}/%{_libdir}

%files
%license COPYRIGHT
%attr(2755,root,mail) %{_bindir}/dotlockfile
%{_libdir}/liblockfile.so.*

%files  devel
%{_libdir}/liblockfile.so
%{_libdir}/liblockfile.a
%{_includedir}/maillock.h
%{_includedir}/lockfile.h

%files  help
%doc README Changelog
%{_mandir}/man1/dotlockfile.1*
%{_mandir}/man3/lockfile_create.3*
%{_mandir}/man3/maillock.3*

%changelog
* Thu Sep 19 2024 Funda Wang <fundawang@yeah.net> - 1.17-3
- cleanup spec

* Tue Aug 20 2024 Bowen Wang<wangbowen@cqsoftware.com.cn> - 1.17-2
- Added 'Buildarch: noarch' to the help subpackage

* Fri Jul 29 2022 wenzhiwei <wenzhiwei@kylinos.cn> - 1.17-1
- Update to 1.17

* Tue Apr 14 2020 huanghaitao <huanghaitao@huawei.com> 1.14-3
- Package init
